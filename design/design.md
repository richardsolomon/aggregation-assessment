## Design decisions

### Tech stack
This application has been developed using Spring Webflux.

Spring Webflux was chosen as it is well suited to the asynchronous and event driven requirements of this service.

For example a request for Prices, Shipments and Tracking information should be queued and batched.
The request should wait for a period of time or until a big enough batch is built up (whichever comes first)
and thereafter run the queries and return the results.

TNT is a large organization with millions of customers thus we can be certain that this service will
receive a very large number of requests.

The application could have been implemented in Spring MVC but because of the thread-per-request model we would 
require significant processing power to handle waiting requests. With Async MVC on Servlet 3.1 we can achieve 
some asynchronous processing by using Async annotations and CompletableFutures, but I find this to be non-ideal
as there's no enforcement to use these and then you could regress to a thread-per-request where HTTP requests 
could block.

With Spring Webflux the HTTP request does not require a thread to wait for the response, instead
the HTTP request is placed in a Queue and once it's processing result is available it will be notified
with a callback and thus reducing the processing power requirements over traditional MVC.

## Components

The 2 most important components in the system are the ApiQueue and the RestFactory.
These handle the query batching requirements and the downstream service integration.

#### Sequence of interactions per request
![Sequence diagram](design.png)