/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.exception.ServiceCallFailedException;
import com.tnt.aggregation.config.RetryConfig;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;

@SpringBootTest(properties = { "app.http.retry.retry-count=3", "app.http.retry.back-off-millis=1" })
class RestFactoryTest {
  public static MockWebServer mockWebServer;

  @Autowired
  private RestFactory restFactory;
  @Autowired
  private RetryConfig retryConfig;

  @BeforeAll
  static void setUp() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();
  }

  @AfterAll
  static void tearDown() throws IOException {
    mockWebServer.shutdown();
  }

  @Test
  void shouldCorrectlyCreateClient() throws Exception {
    var mockBody = "test body";
    var mockRequestUrl = String.format(
      "http://localhost:%d/fooBar",
      mockWebServer.getPort()
    );
    mockWebServer.enqueue(new MockResponse().setBody(mockBody).setResponseCode(200));

    var webClient = restFactory.createClient(mockRequestUrl);
    StepVerifier.create(webClient.get().retrieve().bodyToMono(String.class).log()).assertNext(mockBody::equals).verifyComplete();

    var recordedRequest = mockWebServer.takeRequest();
    assertEquals(
      mockRequestUrl,
      recordedRequest.getRequestUrl().toString()
    );
  }

  @ParameterizedTest
  @EnumSource
  void shouldCorrectlyExecuteGetRequest(ServiceType serviceType) throws Exception {
    var mockBody = "test body";
    var mockRequestUrl = String.format(
      "http://localhost:%d/service%s",
      mockWebServer.getPort(),
      serviceType
    );
    var mockQueryParams = Set.of(
      "one",
      "two",
      "three"
    );
    mockWebServer.enqueue(new MockResponse().setBody(mockBody).setResponseCode(200));

    StepVerifier.create(
      restFactory.get(
        serviceType,
        restFactory.createClient(mockRequestUrl),
        String.class,
        mockQueryParams
      ).log()
    ).assertNext(mockBody::equals).verifyComplete();

    var recordedRequest = mockWebServer.takeRequest();
    assertEquals(
      String.format(
        "%s?%s=%s",
        mockRequestUrl,
        RestFactory.QUERY_PARAM,
        mockQueryParams.stream().collect(Collectors.joining(","))
      ),
      recordedRequest.getRequestUrl().toString()
    );
  }

  @Test
  void shouldCorrectlyRetryGetRequest() {
    var mockBody = "test body";
    var mockRequestUrl = String.format(
      "http://localhost:%d/service",
      mockWebServer.getPort()
    );
    var mockQueryParams = Set.of(
      "one",
      "two",
      "three"
    );

    for (int i = 1; i < retryConfig.getRetryCount(); i++) {
      mockWebServer.enqueue(new MockResponse().setBody("error").setResponseCode(500));
    }
    mockWebServer.enqueue(new MockResponse().setBody(mockBody).setResponseCode(200));

    StepVerifier.create(
      restFactory.get(
        ServiceType.SHIPMENTS,
        restFactory.createClient(mockRequestUrl),
        String.class,
        mockQueryParams
      ).log()
    ).assertNext(mockBody::equals).verifyComplete();
  }

  @Test
  void shouldFailAfterXRetries() {
    var mockRequestUrl = String.format(
      "http://localhost:%d/service",
      mockWebServer.getPort()
    );
    var mockQueryParams = Set.of(
      "one",
      "two",
      "three"
    );

    for (int i = 1; i <= retryConfig.getRetryCount() + 1; i++) {
      mockWebServer.enqueue(new MockResponse().setBody("error").setResponseCode(500));
    }

    StepVerifier.create(
      restFactory.get(
        ServiceType.SHIPMENTS,
        restFactory.createClient(mockRequestUrl),
        String.class,
        mockQueryParams
      )
    ).expectError(ServiceCallFailedException.class).verify();
  }

  @Test
  void shouldNotRetryNon500Requests() {
    var mockRequestUrl = String.format(
      "http://localhost:%d/service",
      mockWebServer.getPort()
    );
    var mockQueryParams = Set.of(
      "one",
      "two",
      "three"
    );

    mockWebServer.enqueue(new MockResponse().setBody("error").setResponseCode(400));

    StepVerifier.create(
      restFactory.get(
        ServiceType.SHIPMENTS,
        restFactory.createClient(mockRequestUrl),
        String.class,
        mockQueryParams
      )
    ).expectError(ServiceCallFailedException.class).verify();
  }
}
