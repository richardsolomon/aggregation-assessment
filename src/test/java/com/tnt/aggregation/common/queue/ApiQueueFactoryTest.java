/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.queue;

import static org.junit.Assert.assertEquals;

import com.tnt.aggregation.common.ServiceType;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;

@SpringBootTest
class ApiQueueFactoryTest {
  @Autowired
  private ApiQueueFactory apiQueueFactory;

  @ParameterizedTest
  @EnumSource
  void shouldCreateOneInstancePerServiceType(ServiceType serviceType) {
    var queue = apiQueueFactory.getQueue(
      serviceType,
      (params) -> Mono.just(keys -> null)
    );
    assertEquals(
      queue,
      apiQueueFactory.getQueue(
        serviceType,
        (params) -> Mono.just(keys -> null)
      )
    );
  }
}
