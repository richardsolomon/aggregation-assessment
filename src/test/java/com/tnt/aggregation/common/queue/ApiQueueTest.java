/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.queue;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.config.QueueConfig;
import com.tnt.aggregation.pricing.PricingRS;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class ApiQueueTest {
  @Test
  void shouldCorrectlyQueueQueries() {
    var queryParams1 = Set.of("NL");
    var queryParams2 = Set.of(
      "NL",
      "CN",
      "DE"
    );
    var queryParams3 = Set.of(
      "NL",
      "CN"
    );

    // just using PricingRS as the return type for sake of convenience vs creating
    // test version
    var mockRs = new PricingRS(
        Map.of(
          "NL",
          14.242090605778d,
          "CN",
          20.503467806384d,
          "DE",
          16.7891234000d
        )
    );

    var apiQueue = new ApiQueue<PricingRS>(new QueueConfig(5, 5000), ServiceType.PRICING, (params) -> Mono.just(mockRs));

    StepVerifier.withVirtualTime(
      () -> Flux.fromIterable(
        Arrays.asList(
          queryParams1,
          queryParams2,
          queryParams3
        )
      ).flatMap(
        queryParams -> Mono.create(
          (sink) -> apiQueue.queueRequest(
            queryParams,
            r -> sink.success(r)
          )
        )
      ).delayElements(Duration.ofSeconds(1))
    ).expectSubscription().expectNoEvent(Duration.ofSeconds(1)).expectNoEvent(Duration.ofSeconds(1))
        .expectNoEvent(Duration.ofSeconds(1)).assertNext(r -> {
          assertInstanceOf(
            PricingRS.class,
            r
          );
          assertEquals(
            queryParams1,
            ((PricingRS) r).getCountryCodes()
          );
        }).assertNext(r -> {
          assertInstanceOf(
            PricingRS.class,
            r
          );
          assertEquals(
            queryParams2,
            ((PricingRS) r).getCountryCodes()
          );
        }).assertNext(r -> {
          assertInstanceOf(
            PricingRS.class,
            r
          );
          assertEquals(
            queryParams3,
            ((PricingRS) r).getCountryCodes()
          );
        }).verifyComplete();
  }

  @Test
  void shouldCorrectlySendQueriesOnTimeout() {
    var queryParams = Set.of("NL");

    // just using PricingRS as the return type for sake of convenience vs creating
    // test version
    var mockRs = new PricingRS(
        Map.of(
          "NL",
          14.242090605778d
        )
    );

    var apiQueue = new ApiQueue<PricingRS>(new QueueConfig(5, 2000), ServiceType.PRICING, (params) -> Mono.just(mockRs));

    StepVerifier.withVirtualTime(
      () -> Mono.create(
        (sink) -> apiQueue.queueRequest(
          queryParams,
          r -> sink.success(r)
        )
      )
    ).expectSubscription().expectNoEvent(Duration.ofSeconds(2)).assertNext(r -> {
      assertInstanceOf(
        PricingRS.class,
        r
      );
      assertEquals(
        queryParams,
        ((PricingRS) r).getCountryCodes()
      );
    }).verifyComplete();
  }
}
