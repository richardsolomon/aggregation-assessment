/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.pricing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PricingRSTest {
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void shouldCorrectlySerializeAndDeserialize() throws Exception {
    var mockCountryCode = "NL";
    var mockPrice = 14.242090605778d;
    var mockRs = new PricingRS(
        Map.of(
          mockCountryCode,
          mockPrice
        )
    );

    var deserializedRs = objectMapper.readValue(
      objectMapper.writeValueAsString(mockRs),
      PricingRS.class
    );
    assertTrue(deserializedRs.getCountryCodes().contains(mockCountryCode));
    assertTrue(deserializedRs.getPrice(mockCountryCode).equals(mockPrice));
  }

  @Test
  void shouldCorrectlyReturnCountryCodesAndLinkedPrices() {
    var mockCountryCode1 = "NL";
    var mockPrice1 = 14.242090605778d;
    var mockCountryCode2 = "CN";
    var mockPrice2 = 20.503467806384d;
    var mockRs = new PricingRS(
        Map.of(
          mockCountryCode1,
          mockPrice1,
          mockCountryCode2,
          mockPrice2
        )
    );

    assertEquals(
      Set.of(
        mockCountryCode1,
        mockCountryCode2
      ),
      mockRs.getCountryCodes()
    );
    assertEquals(
      mockPrice1,
      mockRs.getPrice(mockCountryCode1)
    );
    assertEquals(
      mockPrice2,
      mockRs.getPrice(mockCountryCode2)
    );
  }

  @Test
  void shouldCorrectlySplit() {
    var mockCountryCode1 = "NL";
    var mockPrice1 = 14.242090605778d;
    var mockCountryCode2 = "CN";
    var mockPrice2 = 20.503467806384d;
    var mockCountryCode3 = "DE";
    var mockPrice3 = 14.503467806384d;
    var mockRs = new PricingRS(
        Map.of(
          mockCountryCode1,
          mockPrice1,
          mockCountryCode2,
          mockPrice2,
          mockCountryCode3,
          mockPrice3
        )
    );

    assertEquals(
      Map.of(
        mockCountryCode1,
        mockPrice1
      ),
      mockRs.split(Set.of(mockCountryCode1)).getPricingMap()
    );
    assertEquals(
      Map.of(
        mockCountryCode1,
        mockPrice1,
        mockCountryCode2,
        mockPrice2
      ),
      mockRs.split(
        Set.of(
          mockCountryCode1,
          mockCountryCode2
        )
      ).getPricingMap()
    );
  }
}
