/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.shipments;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ShipmentsRSTest {
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void shouldCorrectlySerializeAndDeserialize() throws Exception {
    var mockOrderNumber = "123456789";
    var mockProduct = "envelope";
    var mockRs = new ShipmentsRS(
        Map.of(
          mockOrderNumber,
          Arrays.asList(mockProduct)
        )
    );

    var deserializedRs = objectMapper.readValue(
      objectMapper.writeValueAsString(mockRs),
      ShipmentsRS.class
    );
    assertTrue(deserializedRs.getOrderNumbers().contains(mockOrderNumber));
    assertTrue(deserializedRs.getProducts(mockOrderNumber).contains(mockProduct));
  }

  @Test
  void shouldCorrectlyReturnOrderNumbersAndLinkedProducts() {
    var mockOrderNumber1 = "123456789";
    var mockProducts1 = Arrays.asList("envelope");
    var mockOrderNumber2 = "987654321";
    var mockProducts2 = Arrays.asList(
      "box",
      "pallet"
    );
    var mockRs = new ShipmentsRS(
        Map.of(
          mockOrderNumber1,
          mockProducts1,
          mockOrderNumber2,
          mockProducts2
        )
    );

    assertEquals(
      Set.of(
        mockOrderNumber1,
        mockOrderNumber2
      ),
      mockRs.getOrderNumbers()
    );
    assertEquals(
      mockProducts1,
      mockRs.getProducts(mockOrderNumber1)
    );
    assertEquals(
      mockProducts2,
      mockRs.getProducts(mockOrderNumber2)
    );
  }

  @Test
  void shouldCorrectlySplit() {
    var mockOrderNumber1 = "123456789";
    var mockProducts1 = Arrays.asList("envelope");
    var mockOrderNumber2 = "987654321";
    var mockProducts2 = Arrays.asList(
      "box",
      "pallet"
    );
    var mockOrderNumber3 = "777777777";
    var mockProducts3 = Arrays.asList(
      "envelope",
      "pallet"
    );
    var mockRs = new ShipmentsRS(
        Map.of(
          mockOrderNumber1,
          mockProducts1,
          mockOrderNumber2,
          mockProducts2,
          mockOrderNumber3,
          mockProducts3
        )
    );

    assertEquals(
      Map.of(
        mockOrderNumber1,
        mockProducts1
      ),
      mockRs.split(Set.of(mockOrderNumber1)).getShipmentMap()
    );
    assertEquals(
      Map.of(
        mockOrderNumber1,
        mockProducts1,
        mockOrderNumber2,
        mockProducts2
      ),
      mockRs.split(
        Set.of(
          mockOrderNumber1,
          mockOrderNumber2
        )
      ).getShipmentMap()
    );
  }
}
