/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.shipments;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.exception.ServiceCallFailedException;
import com.tnt.aggregation.common.rest.RestFactory;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(properties = { "app.queue.query-cap=1" })
class ShipmentsServiceRestImplTest {
  @MockBean
  private RestFactory mockRestFactory;
  @Autowired
  private ShipmentsServiceRestImpl shipmentsService;

  @Test
  void shouldCorrectlyQueryShipments() {
    var serviceTypeCaptor = ArgumentCaptor.forClass(ServiceType.class);
    var webClientCaptor = ArgumentCaptor.forClass(WebClient.class);
    var returnTypeCaptor = ArgumentCaptor.forClass(Class.class);
    var queryParamsCaptor = ArgumentCaptor.forClass(Set.class);

    var mockRs = new ShipmentsRS(
        Map.of(
          "123456789",
          Arrays.asList("envelope")
        )
    );

    when(
      mockRestFactory.get(
        any(),
        any(),
        any(),
        any()
      )
    ).thenReturn(Mono.just(mockRs));

    StepVerifier.create(shipmentsService.queryShipments(mockRs.getOrderNumbers()).log())
        .assertNext(rs -> rs.map(mockRs::equals).orElse(false)).verifyComplete();

    verify(
      mockRestFactory,
      times(1)
    ).get(
      serviceTypeCaptor.capture(),
      webClientCaptor.capture(),
      returnTypeCaptor.capture(),
      queryParamsCaptor.capture()
    );

    assertEquals(
      ServiceType.SHIPMENTS,
      serviceTypeCaptor.getValue()
    );
    assertEquals(
      shipmentsService.webClient(),
      webClientCaptor.getValue()
    );
    assertEquals(
      ShipmentsRS.class,
      returnTypeCaptor.getValue()
    );
    assertEquals(
      mockRs.getOrderNumbers(),
      queryParamsCaptor.getValue()
    );
  }

  @Test
  void shouldReturnEmptyResponseOnError() {
    when(
      mockRestFactory.get(
        any(),
        any(),
        any(),
        any()
      )
    ).thenReturn(Mono.error(new ServiceCallFailedException(":-(", HttpStatus.BAD_REQUEST)));

    StepVerifier.create(shipmentsService.queryShipments(Set.of("123456789")).log()).assertNext(Optional::isEmpty)
        .verifyComplete();
  }
}
