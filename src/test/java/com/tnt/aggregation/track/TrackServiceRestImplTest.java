/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.track;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.exception.ServiceCallFailedException;
import com.tnt.aggregation.common.rest.RestFactory;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(properties = { "app.queue.query-cap=1" })
class TrackServiceRestImplTest {
  @MockBean
  private RestFactory mockRestFactory;
  @Autowired
  private TrackServiceRestImpl trackService;

  @Test
  void shouldCorrectlyQueryTrackingStatuses() {
    var serviceTypeCaptor = ArgumentCaptor.forClass(ServiceType.class);
    var webClientCaptor = ArgumentCaptor.forClass(WebClient.class);
    var returnTypeCaptor = ArgumentCaptor.forClass(Class.class);
    var queryParamsCaptor = ArgumentCaptor.forClass(Set.class);

    var mockRs = new TrackRS(
        Map.of(
          "123456789",
          "NEW"
        )
    );

    when(
      mockRestFactory.get(
        any(),
        any(),
        any(),
        any()
      )
    ).thenReturn(Mono.just(mockRs));

    StepVerifier.create(trackService.queryTrackingStatuses(mockRs.getOrderNumbers()).log())
        .assertNext(rs -> rs.map(mockRs::equals).orElse(false)).verifyComplete();

    verify(
      mockRestFactory,
      times(1)
    ).get(
      serviceTypeCaptor.capture(),
      webClientCaptor.capture(),
      returnTypeCaptor.capture(),
      queryParamsCaptor.capture()
    );

    assertEquals(
      ServiceType.TRACK,
      serviceTypeCaptor.getValue()
    );
    assertEquals(
      trackService.webClient(),
      webClientCaptor.getValue()
    );
    assertEquals(
      TrackRS.class,
      returnTypeCaptor.getValue()
    );
    assertEquals(
      mockRs.getOrderNumbers(),
      queryParamsCaptor.getValue()
    );
  }

  @Test
  void shouldReturnEmptyResponseOnError() {
    when(
      mockRestFactory.get(
        any(),
        any(),
        any(),
        any()
      )
    ).thenReturn(Mono.error(new ServiceCallFailedException(":-(", HttpStatus.BAD_REQUEST)));

    StepVerifier.create(trackService.queryTrackingStatuses(Set.of("123456789")).log()).assertNext(Optional::isEmpty)
        .verifyComplete();
  }
}
