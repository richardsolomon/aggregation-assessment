/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.track;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TrackRSTest {
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void shouldCorrectlySerializeAndDeserialize() throws Exception {
    var mockOrderNumber = "123456789";
    var mockStatus = "NEW";
    var mockRs = new TrackRS(
        Map.of(
          mockOrderNumber,
          mockStatus
        )
    );

    var deserializedRs = objectMapper.readValue(
      objectMapper.writeValueAsString(mockRs),
      TrackRS.class
    );
    assertTrue(deserializedRs.getOrderNumbers().contains(mockOrderNumber));
    assertTrue(deserializedRs.getStatus(mockOrderNumber).equals(mockStatus));
  }

  @Test
  void shouldCorrectlyReturnOrderNumbersAndLinkedProducts() {
    var mockOrderNumber1 = "123456789";
    var mockStatus1 = "NEW";
    var mockOrderNumber2 = "987654321";
    var mockStatus2 = "COLLECTING";
    var mockRs = new TrackRS(
        Map.of(
          mockOrderNumber1,
          mockStatus1,
          mockOrderNumber2,
          mockStatus2
        )
    );

    assertEquals(
      Set.of(
        mockOrderNumber1,
        mockOrderNumber2
      ),
      mockRs.getOrderNumbers()
    );
    assertEquals(
      mockStatus1,
      mockRs.getStatus(mockOrderNumber1)
    );
    assertEquals(
      mockStatus2,
      mockRs.getStatus(mockOrderNumber2)
    );
  }

  @Test
  void shouldCorrectlySplit() {
    var mockOrderNumber1 = "123456789";
    var mockStatus1 = "NEW";
    var mockOrderNumber2 = "987654321";
    var mockStatus2 = "COLLECTING";
    var mockOrderNumber3 = "777777777";
    var mockStatus3 = "DELIVERED";
    var mockRs = new TrackRS(
        Map.of(
          mockOrderNumber1,
          mockStatus1,
          mockOrderNumber2,
          mockStatus2,
          mockOrderNumber3,
          mockStatus3
        )
    );

    assertEquals(
      Map.of(
        mockOrderNumber1,
        mockStatus1
      ),
      mockRs.split(Set.of(mockOrderNumber1)).getTrackMap()
    );
    assertEquals(
      Map.of(
        mockOrderNumber1,
        mockStatus1,
        mockOrderNumber2,
        mockStatus2
      ),
      mockRs.split(
        Set.of(
          mockOrderNumber1,
          mockOrderNumber2
        )
      ).getTrackMap()
    );
  }
}
