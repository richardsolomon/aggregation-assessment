/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import com.tnt.aggregation.pricing.PricingRS;
import com.tnt.aggregation.pricing.PricingService;
import com.tnt.aggregation.shipments.ShipmentsRS;
import com.tnt.aggregation.shipments.ShipmentsService;
import com.tnt.aggregation.track.TrackRS;
import com.tnt.aggregation.track.TrackService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.test.scheduler.VirtualTimeScheduler;

@SpringBootTest
class ApiAggregationServiceImplTest {
  @MockBean
  private PricingService mockPricingService;
  @MockBean
  private ShipmentsService mockShipmentsService;
  @MockBean
  private TrackService mockTrackService;

  @Autowired
  private ApiAggregationServiceImpl apiAggregationService;

  @Test
  void shouldAggregateAllQueriesIntoOneResponseInParallel() {
    var mockPricingRs = new PricingRS(
        Map.of(
          "NL",
          14.242090605778d
        )
    );
    var mockShipmentsRs = new ShipmentsRS(
        Map.of(
          "123456789",
          Arrays.asList(
            "box",
            "pallet"
          )
        )
    );
    var mockTrackRs = new TrackRS(
        Map.of(
          "123456789",
          "NEW"
        )
    );

    var scheduler = VirtualTimeScheduler.getOrSet();

    // setup some variations of return times for the queries on each service
    when(mockPricingService.queryPrices(any())).thenReturn(
      Mono.just(Optional.of(mockPricingRs)).delayElement(
        Duration.ofSeconds(2),
        scheduler
      )
    );
    when(mockShipmentsService.queryShipments(any())).thenReturn(
      Mono.just(Optional.of(mockShipmentsRs)).delayElement(
        Duration.ofSeconds(4),
        scheduler
      )
    );
    when(mockTrackService.queryTrackingStatuses(any())).thenReturn(
      Mono.just(Optional.of(mockTrackRs)).delayElement(
        Duration.ofSeconds(3),
        scheduler
      )
    );

    StepVerifier.withVirtualTime(
      () -> apiAggregationService.query(
        Optional.of(mockPricingRs.getCountryCodes()),
        Optional.of(mockShipmentsRs.getOrderNumbers()),
        Optional.of(mockTrackRs.getOrderNumbers())
      )
    ).expectSubscription().expectNoEvent(Duration.ofSeconds(1)).expectNoEvent(Duration.ofSeconds(1))
        .expectNoEvent(Duration.ofSeconds(1)).thenAwait(Duration.ofSeconds(1))
        // waited 4 seconds which is the longest query wait time configured above
        // all 3 queries result should be ready now
        .expectNext(new ApiRS(mockPricingRs, mockTrackRs, mockShipmentsRs)).verifyComplete();
  }

  @Test
  void shouldPropagateAnyDownstreamExceptions() {
    var mockPricingRs = new PricingRS(
        Map.of(
          "NL",
          14.242090605778d
        )
    );
    var mockShipmentsRs = new ShipmentsRS(
        Map.of(
          "123456789",
          Arrays.asList(
            "box",
            "pallet"
          )
        )
    );
    var mockTrackRs = new TrackRS(
        Map.of(
          "123456789",
          "NEW"
        )
    );

    var scheduler = VirtualTimeScheduler.getOrSet();
    when(mockPricingService.queryPrices(any())).thenReturn(Mono.error(new Exception(";-(")));
    when(mockShipmentsService.queryShipments(any())).thenReturn(
      Mono.just(Optional.of(mockShipmentsRs)).delayElement(
        Duration.ofSeconds(4),
        scheduler
      )
    );
    when(mockTrackService.queryTrackingStatuses(any())).thenReturn(
      Mono.just(Optional.of(mockTrackRs)).delayElement(
        Duration.ofSeconds(3),
        scheduler
      )
    );

    StepVerifier.withVirtualTime(
      () -> apiAggregationService.query(
        Optional.of(mockPricingRs.getCountryCodes()),
        Optional.of(mockShipmentsRs.getOrderNumbers()),
        Optional.of(mockTrackRs.getOrderNumbers())
      )
    ).expectError(Exception.class).verify();
  }
}
