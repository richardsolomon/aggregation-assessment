/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@SpringBootTest
@AutoConfigureWebTestClient
class ApiAggregationControllerTest {
  @MockBean
  private ApiAggregationService apiAggregationService;
  @Autowired
  private WebTestClient webTestClient;

  @Test
  void shouldSuccessfullyReturnApiResponse() {
    var pricingParamsCaptor = ArgumentCaptor.forClass(Optional.class);
    var trackParamsCaptor = ArgumentCaptor.forClass(Optional.class);
    var shipmentsParamsCaptor = ArgumentCaptor.forClass(Optional.class);

    var mockCountryCodes = validCountryCodes();
    var mockOrderNumbers = validOrderNumbers();
    var mockRs = new ApiRS();

    when(
      apiAggregationService.query(
        any(),
        any(),
        any()
      )
    ).thenReturn(Mono.just(mockRs));

    webTestClient.get().uri(
      String.format(
        "/aggregation%s",
        queryString(
          mockCountryCodes,
          mockOrderNumbers,
          mockOrderNumbers
        )
      )
    ).exchange().expectStatus().isEqualTo(HttpStatus.OK).expectBody(ApiRS.class).value(
      rs -> assertEquals(
        mockRs,
        rs
      )
    );

    verify(
      apiAggregationService,
      times(1)
    ).query(
      pricingParamsCaptor.capture(),
      trackParamsCaptor.capture(),
      shipmentsParamsCaptor.capture()
    );
    assertEquals(
      mockCountryCodes,
      pricingParamsCaptor.getValue().get()
    );
    assertEquals(
      mockOrderNumbers,
      trackParamsCaptor.getValue().get()
    );
    assertEquals(
      mockOrderNumbers,
      shipmentsParamsCaptor.getValue().get()
    );
  }

  @Test
  void shouldReturnResponseWithNullsIfNoQueryParamsProvided() {
    var mockRs = new ApiRS();

    when(
      apiAggregationService.query(
        any(),
        any(),
        any()
      )
    ).thenReturn(Mono.just(mockRs));

    webTestClient.get().uri(
      String.format(
        "/aggregation%s",
        queryString(
          Set.of(),
          Set.of(),
          Set.of()
        )
      )
    ).exchange().expectStatus().isEqualTo(HttpStatus.OK).expectBody(ApiRS.class).value(
      rs -> assertEquals(
        mockRs,
        rs
      )
    );
  }

  @ParameterizedTest
  @MethodSource("variationsOfInvalidCountryCodes")
  void shouldCorrectlyValidatePricingQueryParams(Set<String> countryCodes) {
    webTestClient.get().uri(
      String.format(
        "/aggregation%s",
        queryString(
          countryCodes,
          validOrderNumbers(),
          validOrderNumbers()
        )
      )
    ).exchange().expectStatus().isEqualTo(HttpStatus.BAD_REQUEST).expectBody(ErrorRS.class)
        .value(rs -> assertTrue(!rs.getError().isEmpty()));
  }

  @ParameterizedTest
  @MethodSource("variationsOfInvalidOrderNumbers")
  void shouldCorrectlyValidateTrackQueryParams(Set<String> orderNumbers) {
    webTestClient.get().uri(
      String.format(
        "/aggregation%s",
        queryString(
          validCountryCodes(),
          orderNumbers,
          validOrderNumbers()
        )
      )
    ).exchange().expectStatus().isEqualTo(HttpStatus.BAD_REQUEST).expectBody(ErrorRS.class)
        .value(rs -> assertTrue(!rs.getError().isEmpty()));
  }

  @ParameterizedTest
  @MethodSource("variationsOfInvalidOrderNumbers")
  void shouldCorrectlyValidateShipmentsQueryParams(Set<String> orderNumbers) {
    webTestClient.get().uri(
      String.format(
        "/aggregation%s",
        queryString(
          validCountryCodes(),
          validOrderNumbers(),
          orderNumbers
        )
      )
    ).exchange().expectStatus().isEqualTo(HttpStatus.BAD_REQUEST).expectBody(ErrorRS.class)
        .value(rs -> assertTrue(!rs.getError().isEmpty()));
  }

  private static Stream<Arguments> variationsOfInvalidOrderNumbers() {
    return Stream.of(
      Arguments.of(partiallyValidOrderNumbers()),
      Arguments.of(allInvalidOrderNumbers())
    );
  }

  private static Set<String> validOrderNumbers() {
    return Set.of(
      "123456789",
      "987654321"
    );
  }

  private static Set<String> partiallyValidOrderNumbers() {
    return Set.of(
      "123456789",
      "987"
    );
  }

  private static Set<String> allInvalidOrderNumbers() {
    return Set.of(
      "123",
      "987"
    );
  }

  private static Stream<Arguments> variationsOfInvalidCountryCodes() {
    return Stream.of(
      Arguments.of(partiallyValidCountryCodes()),
      Arguments.of(allInvalidCountryCodes())
    );
  }

  private static Set<String> validCountryCodes() {
    return Set.of(
      "NL",
      "CN"
    );
  }

  private static Set<String> partiallyValidCountryCodes() {
    return Set.of(
      "NL",
      "C"
    );
  }

  private static Set<String> allInvalidCountryCodes() {
    return Set.of(
      "N",
      "CCC"
    );
  }

  private String queryString(
    Set<String> pricingCountryCodes,
    Set<String> trackOrderNumbers,
    Set<String> shipmentsOrderNumbers
  ) {
    return String.format(
      "?pricing=%s&track=%s&shipments=%s",
      pricingCountryCodes.stream().collect(Collectors.joining(",")),
      trackOrderNumbers.stream().collect(Collectors.joining(",")),
      shipmentsOrderNumbers.stream().collect(Collectors.joining(","))
    );
  }
}
