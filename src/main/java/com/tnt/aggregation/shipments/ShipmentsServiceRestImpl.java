/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.shipments;

import java.util.Optional;
import java.util.Set;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.queue.ApiQueue;
import com.tnt.aggregation.common.queue.ApiQueueFactory;
import com.tnt.aggregation.common.queue.ApiQueueListener;
import com.tnt.aggregation.common.rest.RestFactory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Rest based shipment API implementation.
 */
@Slf4j
@Service
public class ShipmentsServiceRestImpl implements ShipmentsService {
  private final WebClient webClient;
  private final ApiQueue apiQueue;

  public ShipmentsServiceRestImpl(
      @Value("${app.shipments.url}") String url, RestFactory restFactory, ApiQueueFactory apiQueueFactory
  )
  {
    webClient = restFactory.createClient(url);
    apiQueue = apiQueueFactory.getQueue(
      ServiceType.SHIPMENTS,
      (queryParamValues) -> restFactory.get(
        ServiceType.SHIPMENTS,
        webClient,
        ShipmentsRS.class,
        Set.copyOf(queryParamValues)
      ).onErrorReturn(new ShipmentsRS())
    );
  }

  @Override
  public Mono<Optional<ShipmentsRS>> queryShipments(Set<String> orderNumbers) {
    return Mono.create(sink -> {
      apiQueue.queueRequest(
        orderNumbers,
        (ApiQueueListener<ShipmentsRS>) rs -> {
          log.info(
            "Received result for shipments API query: {}",
            orderNumbers
          );
          sink.success(Optional.of(rs));
        }
      );
    });
  }

  // for unit testing
  WebClient webClient() {
    return webClient;
  }
}
