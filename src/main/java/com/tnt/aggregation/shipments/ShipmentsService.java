/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.shipments;

import java.util.Optional;
import java.util.Set;

import reactor.core.publisher.Mono;

/**
 * Interface definition for handling requests to the shipments API.
 */
public interface ShipmentsService {
  /**
   * Queries shipments by order number.
   *
   * @param orderNumbers set containing nine digit order numbers
   * @return details of the queried shipments if possible otherwise an empty
   *         Optional
   */
  Mono<Optional<ShipmentsRS>> queryShipments(Set<String> orderNumbers);
}
