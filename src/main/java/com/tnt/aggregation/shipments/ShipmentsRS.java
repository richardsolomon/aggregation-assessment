/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.shipments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tnt.aggregation.common.dto.ResultSplitter;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Response structure for the shipments API.
 *
 * Sample response structure:
 *
 * <pre>
 *  {
 *    "109347263": ["box", "box", "pallet"],
 *    "123456891": ["envelope"]
 *  }
 * </pre>
 */
@AllArgsConstructor
@NoArgsConstructor
public class ShipmentsRS implements ResultSplitter {
  /**
   * key is the order number and value is an array of product names
   */
  private Map<String, List<String>> shipmentMap = new HashMap<>();

  @JsonAnySetter
  public void add(
    String orderNumber,
    List<String> products
  ) {
    shipmentMap.put(
      orderNumber,
      products
    );
  }

  @JsonAnyGetter
  public Map<String, List<String>> getShipmentMap() {
    return shipmentMap;
  }

  @JsonIgnore
  public Set<String> getOrderNumbers() {
    return shipmentMap.keySet();
  }

  @JsonIgnore
  public List<String> getProducts(String orderNumber) {
    return shipmentMap.get(orderNumber);
  }

  @JsonIgnore
  @Override
  public ShipmentsRS split(Set<String> countryCodes) {
    var newShipmentMap = new HashMap(shipmentMap);
    newShipmentMap.keySet().retainAll(countryCodes);
    return new ShipmentsRS(newShipmentMap);
  }
}
