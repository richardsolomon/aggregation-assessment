/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * To be used in instances where a service is responding with 5xx errors or is
 * not reachable.
 */
public class ServiceUnavailableException extends Exception {
  @Getter
  private HttpStatus status;

  public ServiceUnavailableException(String message, HttpStatus status) {
    super(message);
    this.status = status;
  }
}
