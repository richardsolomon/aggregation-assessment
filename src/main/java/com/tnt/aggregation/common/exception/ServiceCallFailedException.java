/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * To be used in instances where the service call cannot be completed
 * successfully, either because of unrecoverable HTTP codes i.e. 400 or from a
 * retry exhaustion.
 */
public class ServiceCallFailedException extends RuntimeException {
  @Getter
  private HttpStatus status;

  public ServiceCallFailedException(String message, HttpStatus status) {
    super(message);
    this.status = status;
  }

  public ServiceCallFailedException(String message, Throwable t) {
    super(message, t);
  }
}
