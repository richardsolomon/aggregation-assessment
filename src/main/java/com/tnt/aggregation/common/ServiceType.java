/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common;

/**
 * An enum to represent the type of downstream API service used by this
 * application.
 *
 * At this point in time used for adding service specific logging information in
 * generic methods.
 */
public enum ServiceType {
  SHIPMENTS, TRACK, PRICING
}
