/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.dto;

import java.util.Set;

/**
 * Interface contract create a copy of an instance split by some keys.
 *
 * Example: - an object instance with a map of {"one" -> 1, "two", 2, "three" ->
 * 3} - when split by {"one", "three"} will return a new instance with {"one" ->
 * 1, "three" -> 3}
 */
public interface ResultSplitter {
  /**
   * @param keys set of keys to use for constructing a new instance from the
   *             current instance
   * @return new instance with subset of the keys passed as a parameter
   */
  ResultSplitter split(Set<String> keys);
}
