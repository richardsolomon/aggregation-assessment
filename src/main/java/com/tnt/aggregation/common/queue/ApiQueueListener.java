/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.queue;

import com.tnt.aggregation.common.dto.ResultSplitter;

/**
 * Users of the ApiQueue can use the listener to receive a callback once the
 * item has been processed.
 *
 * @param <T> instance of ResultSplitter
 */
public interface ApiQueueListener<T extends ResultSplitter> {
  /**
   * callback with the result of the queue operation.
   *
   * @param rs
   */
  void onResponseAvailable(T rs);
}
