/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.queue;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Value;

/**
 * Holds some context information for items waiting in the api queue.
 */
@AllArgsConstructor
@Value
public class ApiQueueItem {
  private Set<String> queryParamValues;
  private ApiQueueListener listener;
  private long queuedTimeMillis;
}
