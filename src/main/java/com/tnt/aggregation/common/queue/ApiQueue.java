/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.queue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.dto.ResultSplitter;
import com.tnt.aggregation.config.QueueConfig;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Implements a FIFO queue that batches queries and once a threshold is reached
 * the oldest items up to that threshold are grouped together and processed.
 *
 * Note: the number of queries may be larger than the threshold in cases when
 * un-even requests push the queries over the threshold. i.e. consider a
 * threshold of 5 - request 1 - queries("NL", "DE") <-- operation waits as #
 * number of queries == 2 - request 2 - queries("NL", "CN") <-- operation waits
 * as # number of queries == 4 - request 3 - queries("NL", "DE", "CN") <--
 * operations executes because threshold breached and runs 7 queries
 *
 * @param <T> sub-type of ResultSplitter
 */
@Slf4j
public class ApiQueue<T extends ResultSplitter> {
  private final AtomicInteger queryItemCount = new AtomicInteger();
  private final ConcurrentLinkedQueue<ApiQueueItem> queue = new ConcurrentLinkedQueue();
  /**
   * a stream that peeks at the oldest item in the queue every second
   */
  private final Flux<Optional<ApiQueueItem>> itemTimeoutProcessor = Flux
      .fromStream(Stream.generate(() -> Optional.ofNullable(queue.peek()))).delayElements(Duration.ofSeconds(1));

  private final QueueConfig queueConfig;
  private final ServiceType serviceType;
  private final Function<Set<String>, Mono<T>> queryFunction;

  ApiQueue(QueueConfig queueConfig, ServiceType serviceType, Function<Set<String>, Mono<T>> queryFunction) {
    this.queueConfig = queueConfig;
    this.serviceType = serviceType;
    this.queryFunction = queryFunction;

    initQueueTimeoutProcessor();
  }

  private void initQueueTimeoutProcessor() {
    // the stream may produce an item and in that case we check the item age
    // if this is older than our maximum wait time then we issue a queue flush
    this.itemTimeoutProcessor.subscribe(maybeItem -> maybeItem.ifPresent(item -> {
      var waitingTime = System.currentTimeMillis() - item.getQueuedTimeMillis();
      if (waitingTime >= queueConfig.getMaxWaitTimeMillis()) {
        log.info(
          "Maximum wait time of {} milliseconds for {} queue reached, flushing waiting requests...",
          queueConfig.getMaxWaitTimeMillis(),
          serviceType
        );
        processQueuedItems();
      }
    }));
  }

  /**
   * Queues a request for queries until a threshold has been reached and then
   * notifies the caller via the supplied listener.
   *
   * @param queryParamValues queries to run
   * @param listener         callback
   */
  public void queueRequest(
    Set<String> queryParamValues,
    ApiQueueListener listener
  ) {
    queue.add(new ApiQueueItem(queryParamValues, listener, System.currentTimeMillis()));
    var count = queryItemCount.addAndGet(queryParamValues.size());

    log.debug(
      "Number of request items in the {} queue: {}",
      serviceType,
      count
    );

    if (count >= queueConfig.getQueryCap()) {
      log.info(
        "{} API queue threshold of {} reached: {}",
        serviceType,
        queueConfig.getQueryCap(),
        count
      );
      processQueuedItems();
    }
  }

  private void processQueuedItems() {
    ApiQueueItem item;
    var processedCount = 0;
    var queryParamsValuesToUse = new HashSet<String>();
    var itemsToNotify = new ArrayList<ApiQueueItem>();
    /*
     * RS note: my understanding of the requirements here is maybe incorrect...
     * there could be significantly more items requested than the cap i.e. 1000 vs 5
     * cap and instead of sending all 1000 in 1 request I have batched these into
     * roughly what the cap size is rather than a monster request - this can easily
     * be changed if incorrect
     */
    while (processedCount <= queueConfig.getQueryCap() && (item = queue.poll()) != null) {
      queryItemCount.addAndGet(item.getQueryParamValues().size() * -1);
      processedCount += item.getQueryParamValues().size();
      queryParamsValuesToUse.addAll(item.getQueryParamValues());
      itemsToNotify.add(item);
    }

    log.debug(
      "Performing {} API query with params: {}",
      serviceType,
      queryParamsValuesToUse
    );
    queryFunction.apply(queryParamsValuesToUse).map(rs -> {
      log.info(
        "{} API query completed",
        serviceType
      );
      itemsToNotify.forEach(i -> i.getListener().onResponseAvailable(rs.split(i.getQueryParamValues())));
      return Mono.empty();
    }).subscribe();
  }
}
