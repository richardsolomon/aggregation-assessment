/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.dto.ResultSplitter;
import com.tnt.aggregation.config.QueueConfig;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Manages the creation of ApiQueue instances with some parameters that are not
 * injectable.
 */
@RequiredArgsConstructor
@Component
public class ApiQueueFactory {
  private final Map<ServiceType, ApiQueue> apiQueues = new HashMap<>();
  private final QueueConfig queueConfig;

  /**
   * Create an instance of ApiQueue for a given ServiceType. Should one have
   * already been created then that instance will be returned.
   *
   * @param serviceType   service type relevant for this ApiQueue
   * @param queryFunction function to execute by the ApiQueue to retrieve items of
   *                      type T
   * @param <T>           subtype of ResultSplitter
   * @return instance of the ApiQueue for the service type
   */
  public <T extends ResultSplitter> ApiQueue<T> getQueue(
    ServiceType serviceType,
    Function<Set<String>, Mono<T>> queryFunction
  ) {
    return apiQueues.computeIfAbsent(
      serviceType,
      k -> new ApiQueue<>(queueConfig, k, queryFunction)
    );
  }
}
