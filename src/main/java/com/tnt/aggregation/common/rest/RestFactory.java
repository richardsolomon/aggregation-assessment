/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.common.rest;

import java.time.Duration;
import java.util.Set;
import java.util.stream.Collectors;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.exception.ServiceCallFailedException;
import com.tnt.aggregation.common.exception.ServiceUnavailableException;
import com.tnt.aggregation.config.RetryConfig;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

/**
 * Shared component used for all downstream service API calls.
 * <p>
 * Handles the creation of WebClient instances and the orchestration of GET
 * requests to those services.
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class RestFactory {
  // all services have the same query param at this point
  public static final String QUERY_PARAM = "q";

  private final RetryConfig retryConfig;

  /**
   * Creates a default WebClient with application specific global settings.
   *
   * @param url full URL of the service to call
   * @return a pre-configured WebClient
   */
  public WebClient createClient(String url) {
    return WebClient.builder().baseUrl(url) // for now only setting a base URL but could add other settings in
        // future
        .build();
  }

  /**
   * Queries a downstream API service with a pre-configured WebClient.
   *
   * @param serviceType      the downstream service type
   * @param webClient        the pre-configured WebClient
   * @param returnType       the class to deserialize the response into
   * @param queryParamValues a Set containing all the query parameters to use,
   *                         these will be converted to a comma delimited string
   * @return an instance of the class that the response is deserialized into
   */
  public <T> Mono<T> get(
    ServiceType serviceType,
    WebClient webClient,
    Class<T> returnType,
    Set<String> queryParamValues
  ) {
    return webClient.get().uri(
      uriBuilder -> uriBuilder.queryParam(
        QUERY_PARAM,
        queryParamValues.stream().collect(Collectors.joining(","))
      ).build()
    ).retrieve().onStatus(
      HttpStatus::is5xxServerError,
      response ->
      // only retrying 500 errors at the moment
      Mono.error(
        new ServiceUnavailableException(
            String.format(
              "%s API is not available. Responded with status code %d",
              serviceType,
              response.rawStatusCode()
            ), response.statusCode()
        )
      )
    ).onStatus(
      status -> !HttpStatus.OK.equals(status) && !status.is5xxServerError(),
      response -> Mono.error(
        new ServiceCallFailedException(
            String.format(
              "%s API call failed. Responded with status code %d",
              serviceType,
              response.rawStatusCode()
            ), response.statusCode()
        )
      )
    ).bodyToMono(returnType).retryWhen(
      Retry.backoff(
        retryConfig.getRetryCount(),
        Duration.ofMillis(retryConfig.getBackOffMillis())
      ).filter(throwable -> throwable instanceof ServiceUnavailableException)
          .doAfterRetry(
            retrySignal -> log.info(
              "Retrying {} API call - {} of {}...",
              serviceType,
              (retrySignal.totalRetries() + 1),
              retryConfig.getRetryCount()
            )
          ).onRetryExhaustedThrow(
            (
              retryBackoffSpec,
              retrySignal
            ) -> {
              log.error(
                "%s API call failed.",
                retrySignal.failure()
              );
              throw new ServiceCallFailedException(
                  String.format(
                    "%s API call failed after %d retries",
                    serviceType,
                    retrySignal.totalRetries()
                  ), retrySignal.failure()
              );
            }
          )
    ).doOnError(
      throwable -> log.error(
        "Error occurred whilst handling the request",
        throwable
      )
    );
  }
}
