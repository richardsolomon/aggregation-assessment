/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.pricing;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.tnt.aggregation.common.dto.ResultSplitter;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Response structure for the pricing API.
 *
 * Sample response structure:
 *
 * <pre>
 *  {
 *    "NL": 14.242090605778
 *    "CN": 20.503467806384
 *  }
 * </pre>
 */
@AllArgsConstructor
@NoArgsConstructor
public class PricingRS implements ResultSplitter {
  /**
   * key is the ISO-2 country code and value is the price
   */
  private Map<String, Double> pricingMap = new HashMap<>();

  @JsonAnySetter
  public void add(
    String countryCode,
    Double price
  ) {
    pricingMap.put(
      countryCode,
      price
    );
  }

  @JsonAnyGetter
  public Map<String, Double> getPricingMap() {
    return pricingMap;
  }

  @JsonIgnore
  public Set<String> getCountryCodes() {
    return pricingMap.keySet();
  }

  @JsonIgnore
  public Double getPrice(String countryCode) {
    return pricingMap.get(countryCode);
  }

  @JsonIgnore
  @Override
  public PricingRS split(Set<String> countryCodes) {
    var newPricingMap = new HashMap(pricingMap);
    newPricingMap.keySet().retainAll(countryCodes);
    return new PricingRS(newPricingMap);
  }
}
