/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.pricing;

import java.util.Optional;
import java.util.Set;

import reactor.core.publisher.Mono;

/**
 * Interface definition for handling requests to the pricing API.
 */
public interface PricingService {
  /**
   * Queries shipments by order number.
   *
   * @param countryCodes ISO-2 country code
   * @return shipping price by country code if possible otherwise an empty
   *         Optional
   */
  Mono<Optional<PricingRS>> queryPrices(Set<String> countryCodes);
}
