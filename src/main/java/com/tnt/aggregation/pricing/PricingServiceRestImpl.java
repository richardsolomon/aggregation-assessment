/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.pricing;

import java.util.Optional;
import java.util.Set;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.queue.ApiQueue;
import com.tnt.aggregation.common.queue.ApiQueueFactory;
import com.tnt.aggregation.common.queue.ApiQueueListener;
import com.tnt.aggregation.common.rest.RestFactory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Rest based pricing API implementation.
 */
@Slf4j
@Service
public class PricingServiceRestImpl implements PricingService {
  private final WebClient webClient;
  private final ApiQueue apiQueue;

  public PricingServiceRestImpl(
      @Value("${app.pricing.url}") String url, RestFactory restFactory, ApiQueueFactory apiQueueFactory
  )
  {
    webClient = restFactory.createClient(url);
    apiQueue = apiQueueFactory.getQueue(
      ServiceType.PRICING,
      (queryParamValues) -> restFactory.get(
        ServiceType.PRICING,
        webClient,
        PricingRS.class,
        Set.copyOf(queryParamValues)
      ).onErrorReturn(new PricingRS())
    );
  }

  @Override
  public Mono<Optional<PricingRS>> queryPrices(Set<String> countryCodes) {
    return Mono.create(sink -> {
      apiQueue.queueRequest(
        countryCodes,
        (ApiQueueListener<PricingRS>) rs -> {
          log.info(
            "Received result for prices API query: {}",
            countryCodes
          );
          sink.success(Optional.of(rs));
        }
      );
    });
  }

  // for unit testing
  WebClient webClient() {
    return webClient;
  }
}
