/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Represents the HTTP retry strategy configuration.
 *
 * Sample configuration shown below:
 *
 * <pre>
 *  app:
 *    http:
 *      retry:
 *        retry-count: 3
 *        back-off-millis: 500
 * </pre>
 */
@Configuration
@ConfigurationProperties(prefix = "app.http.retry")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetryConfig {
  private int retryCount;
  private long backOffMillis;
}
