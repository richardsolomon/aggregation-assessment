/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Represents the downstream API queue strategy configuration.
 *
 * Sample configuration shown below:
 *
 * <pre>
 *  app:
 *    queue:
 *      query-cap: 5
 *      max-wait-time-millis: 5000
 * </pre>
 */
@Configuration
@ConfigurationProperties(prefix = "app.queue")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueueConfig {
  private int queryCap;
  private long maxWaitTimeMillis;
}
