/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(title = "TNT API Aggregation Service", version = "0.1", description = "API documentation for the TNT API aggregation service"))
@SpringBootApplication
public class App {
  public static void main(String[] args) {
    SpringApplication.run(
      App.class,
      args
    );
  }
}
