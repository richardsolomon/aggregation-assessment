/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.track;

import java.util.Optional;
import java.util.Set;

import reactor.core.publisher.Mono;

/**
 * Interface definition for handling requests to the track API.
 */
public interface TrackService {
  /**
   * Queries tracking status by order number.
   *
   * @param orderNumbers nine digit order number
   * @return status of order numbers if possible otherwise an empty Optional
   */
  Mono<Optional<TrackRS>> queryTrackingStatuses(Set<String> orderNumbers);
}
