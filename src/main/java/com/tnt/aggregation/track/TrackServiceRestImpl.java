/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.track;

import java.util.Optional;
import java.util.Set;

import com.tnt.aggregation.common.ServiceType;
import com.tnt.aggregation.common.queue.ApiQueue;
import com.tnt.aggregation.common.queue.ApiQueueFactory;
import com.tnt.aggregation.common.queue.ApiQueueListener;
import com.tnt.aggregation.common.rest.RestFactory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Rest based track API implementation.
 */
@Slf4j
@Service
public class TrackServiceRestImpl implements TrackService {
  private final WebClient webClient;
  private final ApiQueue apiQueue;

  public TrackServiceRestImpl(@Value("${app.track.url}") String url, RestFactory restFactory, ApiQueueFactory apiQueueFactory) {
    webClient = restFactory.createClient(url);
    apiQueue = apiQueueFactory.getQueue(
      ServiceType.TRACK,
      (queryParamValues) -> restFactory.get(
        ServiceType.TRACK,
        webClient,
        TrackRS.class,
        Set.copyOf(queryParamValues)
      ).onErrorReturn(new TrackRS())
    );
  }

  @Override
  public Mono<Optional<TrackRS>> queryTrackingStatuses(Set<String> orderNumbers) {
    return Mono.create(sink -> {
      apiQueue.queueRequest(
        orderNumbers,
        (ApiQueueListener<TrackRS>) rs -> {
          log.info(
            "Received result for track API query: {}",
            orderNumbers
          );
          sink.success(Optional.of(rs));
        }
      );
    });
  }

  // for unit testing
  WebClient webClient() {
    return webClient;
  }
}
