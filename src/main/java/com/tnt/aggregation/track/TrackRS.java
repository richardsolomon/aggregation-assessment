/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.track;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.tnt.aggregation.common.dto.ResultSplitter;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Response structure for the track API.
 *
 * Sample response structure:
 *
 * <pre>
 *  {
 *    "109347263": "NEW",
 *    "123456891": "COLLECTING"
 *  }
 * </pre>
 */
@AllArgsConstructor
@NoArgsConstructor
public class TrackRS implements ResultSplitter {
  /**
   * key is the order number and value is the status
   */
  private Map<String, String> trackMap = new HashMap<>();

  @JsonAnySetter
  public void add(
    String orderNumber,
    String status
  ) {
    trackMap.put(
      orderNumber,
      status
    );
  }

  @JsonAnyGetter
  public Map<String, String> getTrackMap() {
    return trackMap;
  }

  @JsonIgnore
  public Set<String> getOrderNumbers() {
    return trackMap.keySet();
  }

  @JsonIgnore
  public String getStatus(String orderNumber) {
    return trackMap.get(orderNumber);
  }

  @JsonIgnore
  @Override
  public TrackRS split(Set<String> countryCodes) {
    var newTrackMap = new HashMap(trackMap);
    newTrackMap.keySet().retainAll(countryCodes);
    return new TrackRS(newTrackMap);
  }
}
