/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import java.util.Optional;
import java.util.Set;

import reactor.core.publisher.Mono;

/**
 * Interface definition for receiving a query request for pricing, tracking
 * statuses and shipment information.
 */
public interface ApiAggregationService {
  /**
   * Should return a consolidated response of pricing, tracking and shipping
   * information.
   *
   * @param pricingQueries   an optional Set of country codes to obtain prices for
   * @param trackQueries     an optional Set of order numbers to obtain tracking
   *                         statuses for
   * @param shipmentsQueries an optional Set of order numbers to obtain shipment
   *                         information for
   * @return consolidated response of pricing, tracking and shipping information
   */
  Mono<ApiRS> query(
    Optional<Set<String>> pricingQueries,
    Optional<Set<String>> trackQueries,
    Optional<Set<String>> shipmentsQueries
  );
}
