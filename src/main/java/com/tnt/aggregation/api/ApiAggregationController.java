/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Validated
@RequiredArgsConstructor
@RequestMapping("/aggregation")
@RestController
public class ApiAggregationController {
  private final ApiAggregationService apiAggregationService;

  @Operation(description = "API aggregation query endpoint", parameters = {
      @Parameter(name = "pricing", in = ParameterIn.QUERY, required = false, description = "Comma separated list of ISO-2 country codes"),
      @Parameter(name = "track", in = ParameterIn.QUERY, required = false, description = "Comma separated list of order numbers"),
      @Parameter(name = "shipments", in = ParameterIn.QUERY, required = false, description = "Comma separated list of order numbers") }, responses = {
          @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = ApiRS.class))),
          @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation = ErrorRS.class))),
          @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation = ErrorRS.class))) })
  @GetMapping
  public Mono<ApiRS> query(
    @RequestParam("pricing") Optional<Set<@Size(min = 2, max = 2, message = "Country codes should be 2 characters in length") String>> pricingQueries,
    @RequestParam("track") Optional<Set<@Size(min = 9, max = 9, message = "Order numbers should be 9 digits in length") String>> trackQueries,
    @RequestParam("shipments") Optional<Set<@Size(min = 9, max = 9, message = "Order numbers should be 9 digits in length") String>> shipmentsQueries
  ) {
    return apiAggregationService.query(
      pricingQueries,
      trackQueries,
      shipmentsQueries
    );
  }
}
