/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Generic error response for the application.
 *
 * Sample response for a 400 error:
 *
 * <pre>
 *  {
 *    "error": "Invalid xyz"
 *  }
 * </pre>
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ErrorRS {
  private String error;
}
