/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

/**
 * Catches and manages all exceptions thrown by the application.
 */
@RestControllerAdvice
public class ApiExceptionHandler {
  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  public Mono<ErrorRS> handleBadRequestErrors(ConstraintViolationException ex) {
    return Mono.just(
      new ErrorRS(
          ex.getConstraintViolations().stream().map(
            v -> String.format(
              "%s: %s",
              v.getMessage(),
              v.getInvalidValue()
            )
          ).collect(Collectors.joining(". "))
      )
    );
  }

  @ExceptionHandler({ Exception.class, IllegalArgumentException.class })
  @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
  public Mono<ErrorRS> handleUnknownError(Exception ex) {
    return Mono.just(new ErrorRS("Unknown error has occurred."));
  }
}
