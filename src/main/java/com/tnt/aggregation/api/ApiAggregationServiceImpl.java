/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import java.util.Optional;
import java.util.Set;

import com.tnt.aggregation.pricing.PricingService;
import com.tnt.aggregation.shipments.ShipmentsService;
import com.tnt.aggregation.track.TrackService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class ApiAggregationServiceImpl implements ApiAggregationService {
  private final PricingService pricingService;
  private final TrackService trackService;
  private final ShipmentsService shipmentsService;

  @Override
  public Mono<ApiRS> query(
    Optional<Set<String>> pricingQueries,
    Optional<Set<String>> trackQueries,
    Optional<Set<String>> shipmentsQueries
  ) {
    return Mono.zip(
      pricingQueries.map(pricingService::queryPrices).orElse(Mono.just(Optional.empty())),
      trackQueries.map(trackService::queryTrackingStatuses).orElse(Mono.just(Optional.empty())),
      shipmentsQueries.map(shipmentsService::queryShipments).orElse(Mono.just(Optional.empty()))
    ).map(tuple -> new ApiRS(tuple.getT1(), tuple.getT2(), tuple.getT3())).switchIfEmpty(Mono.just(new ApiRS()));
  }
}
