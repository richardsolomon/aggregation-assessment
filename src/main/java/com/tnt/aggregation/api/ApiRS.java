/* Copyright 2022 TNT... <TODO> */
package com.tnt.aggregation.api;

import java.util.Optional;

import com.tnt.aggregation.pricing.PricingRS;
import com.tnt.aggregation.shipments.ShipmentsRS;
import com.tnt.aggregation.track.TrackRS;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Consolidated API response structure containing the query results from the
 * pricing, track and shipments services.
 *
 * Sample response:
 *
 * <pre>
 *  {
 *    "pricing": {
 *      "NL": 14.242090605778
 *      "CN": 20.503467806384
 *    },
 *    "track": {
 *      "109347263": null,
 *      "123456891": "COLLECTING"
 *    },
 *    "shipments": {
 *      "109347263": ["box", "box", "pallet"],
 *      "123456891": null
 *    }
 *  }
 * </pre>
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApiRS {
  private PricingRS pricing;
  private TrackRS track;
  private ShipmentsRS shipments;

  /**
   * Some API's may return an empty Optional, this constructor converts those
   * empty Optionals to null for the required response value.
   */
  public ApiRS(Optional<PricingRS> pricing, Optional<TrackRS> track, Optional<ShipmentsRS> shipments) {
    this.pricing = pricing.orElse(null);
    this.track = track.orElse(null);
    this.shipments = shipments.orElse(null);
  }
}
