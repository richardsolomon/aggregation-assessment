# API Aggregation Service

Consolidates requests to the Pricing, Track and Shipments services.

This project has been written using Spring Webflux

## Running the application

You can run the application with docker compose using the following command:
```shell script
docker-compose up
```

## Building the application

You can build the application with gradle using the following command:
```shell script
./gradlew build
```

The build will run tests and also perform a code format check on the source files. If the code format check fails
you can run the following to fix any formatting issues:
```shell script
./gradlew spotlessApply
```

## Designs and decisions
For some background on the designs and decisions [click here](design/design.md). 

## Sample request/response
```
GET http://localhost:8081/aggregation?pricing=NL,CN&track=109347263,123456891&shipments=109347263,123456891
200 OK
content-type: application/json
{
  "pricing": {
    "NL": 14.242090605778
    "CN": 20.503467806384
  },
  "track": {
    "109347263": null,
    "123456891": "COLLECTING"
  },
  "shipments": {
    "109347263": ["box", "box", "pallet"],
    "123456891": null
  }
}
```

## Swagger docs

Once the application is running you can access these here: http://localhost:8081/webjars/swagger-ui/index.html
